/*
 * isdup.c: Implementation of isdup.h.
 */

#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <stdlib.h>

#define __USE_GNU

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>
#include <fcntl.h>

#include "isdup.h"

#if defined TEST_ISDUP || defined STANDALONE_ISDUP
#define GIVE_REASON
#define REASONS(X) \
    X(REASON_NONE, "no reason available")        \
    X(REASON_DEV_INO, "fds have different (device,inode) pairs")        \
    X(REASON_GETFL, "fds have different F_GETFL flags")                 \
    X(REASON_FLOCK_POS, "LOCK_EX on both fds at once succeeds")         \
    X(REASON_FLOCK_NEG, "LOCK_EX on both fds at once fails")            \
    X(REASON_FLOCK_INCONCLUSIVE, "unable to LOCK_EX one of the fds")    \
    X(REASON_NONBLOCK_POS, "changing O_NONBLOCK on one fd seems to change " \
      "the other")                                                      \
    X(REASON_NONBLOCK_NEG, "changing O_NONBLOCK on one fd did not reliably " \
      "change the other")                                               \
    /* end of list */
#define REASONS_ENUM(id, str) id,
typedef enum { REASONS(REASONS_ENUM) N_REASONS } Reason;
Reason reason;
#endif

int isdup(int fd1, int fd2, int approx)
{
    int ret, fl1, fl2, i, hits;
    struct stat st1, st2;

#ifdef GIVE_REASON
    reason = REASON_NONE;
#endif

    /*
     * To begin with, weed out obvious non-duplicates by using
     * fstat and F_GETFL.
     */
    if (fstat(fd1, &st1) == 0 && fstat(fd2, &st2) == 0) {
	if (st1.st_dev != st2.st_dev ||
	    st1.st_ino != st2.st_ino) {
#ifdef GIVE_REASON
	    reason = REASON_DEV_INO;
#endif
	    return 0;		       /* different files => not duplicates */
	}
    }
    if ((fl1 = fcntl(fd1, F_GETFL)) >= 0 &&
	(fl2 = fcntl(fd2, F_GETFL)) >= 0) {
	/*
	 * Race conditions: another process might have done
	 * F_SETFL in between those two calls. The Linux man page
	 * for fcntl says that the only flags settable by F_SETFL
	 * are O_APPEND, O_NONBLOCK, O_ASYNC, and O_DIRECT. So we
	 * exclude those from consideration, and check everything
	 * else.
	 */
	fl1 &= ~(O_APPEND | O_NONBLOCK | O_ASYNC | O_DIRECT);
	fl2 &= ~(O_APPEND | O_NONBLOCK | O_ASYNC | O_DIRECT);
	if (fl1 != fl2) {
#ifdef GIVE_REASON
	    reason = REASON_GETFL;
#endif
	    return 0;		       /* not duplicates */
	}
    }

    /*
     * The most reliable way to determine duplicateness is using
     * flock. An flock() lock is associated with an open file; so
     * if we can successfully take out an exclusive lock on fd1,
     * then a subsequent attempt to exclusively lock fd2 will
     * succeed if and only if fd1 and fd2 are duplicates.
     *
     * (This is assuming, of course, that our fstat work above has
     * already disposed of the case where fd1 and fd2 are not even
     * pointing to the same _file_. Naturally if they're different
     * files we can exclusively lock each of them independently!
     * But if they're the same file, and we lock one of them, then
     * we can lock the other iff the fds are duplicates.)
     */
    ret = flock(fd1, LOCK_EX | LOCK_NB);
    if (ret == 0) {
	ret = flock(fd2, LOCK_EX | LOCK_NB);
	flock(fd1, LOCK_UN | LOCK_NB);

        if (ret == 0) {
#ifdef GIVE_REASON
            reason = REASON_FLOCK_POS;
#endif
            return 1;
        } else {
#ifdef GIVE_REASON
            reason = REASON_FLOCK_NEG;
#endif
            return 0;
        }
    }

    /*
     * The above will be inconclusive if we weren't able to
     * exclusively lock fd1 in the first place, perhaps because
     * another process had the file locked already. In that case,
     * we must fall back to approximate methods.
     */
    if (!approx) {
#ifdef GIVE_REASON
	reason = REASON_FLOCK_INCONCLUSIVE;
#endif
	return -1;		       /* couldn't say for sure */
    }

    /*
     * The nonblocking status of a file is a property of the open
     * file. So switch it back and forth randomly on one fd for a
     * bit and see whether the other fd keeps pace.
     * 
     * This is subject to race conditions (if another process is
     * changing the nonblocking status of the file for its own
     * purposes) and to deliberate subversion (if another process
     * by dint of great effort is duplicating our changes to one
     * fd into the other in an effort to convince us they're the
     * same). However, in the absence of outside interference it's
     * reliable.
     */
    fl1 = fcntl(fd1, F_GETFL);
    hits = 0;
    for (i = 0; i < 128; i++) {
	int r = rand();
	fl2 = fl1 & ~O_NONBLOCK;
	if (r >= RAND_MAX/2)
	    fl2 |= O_NONBLOCK;
	fcntl(fd1, F_SETFL, fl2);
	if (fcntl(fd2, F_GETFL) == fl2)
	    hits++;
    }
    fcntl(fd1, F_SETFL, fl1);
    if (hits == i) {
#ifdef GIVE_REASON
        reason = REASON_NONBLOCK_POS;
#endif
        return 1;
    } else {
#ifdef GIVE_REASON
        reason = REASON_NONBLOCK_NEG;
#endif
        return 0;
    }
}

#ifdef TEST_ISDUP

/*
gcc -DTEST_ISDUP -o test_isdup isdup.c && ./test_isdup
*/

#define TEST(a, rel, b) do { \
    int aval = a; \
    int bval = b; \
    if (aval rel bval) \
	passes++; \
    else { \
	printf("FAIL: line %d: %s == %d (not %s %d)\n", \
		   __LINE__, #a, aval, #rel, bval); \
	fails++; \
    } \
} while (0)

int main(void)
{
    char filename[40];
    char filename2[40];
    int passes = 0, fails = 0;
    int fd1, fd2, fd3, fd4;

    strcpy(filename, "/tmp/isduptestXXXXXX");
    if (!mkstemp(filename)) {
	perror("mkstemp");
	return 1;
    }

    strcpy(filename2, "/tmp/isduptestXXXXXX");
    if (!mkstemp(filename2)) {
	perror("mkstemp");
	return 1;
    }

    /*
     * Test REASON_DEV_INO.
     */
    fd1 = open(filename, O_RDONLY);
    fd2 = open(filename, O_RDONLY);
    fd3 = open(filename2, O_RDONLY);
    TEST(isdup(fd1, fd2, 0), ==, 0);
    TEST(reason, >, REASON_DEV_INO);
    TEST(isdup(fd1, fd3, 0), ==, 0);
    TEST(reason, ==, REASON_DEV_INO);
    close(fd1);
    close(fd2);
    close(fd3);

    /*
     * Test REASON_GETFL.
     */
    fd1 = open(filename, O_RDONLY);
    fd2 = open(filename, O_RDONLY);
    fd3 = open(filename, O_RDWR);
    TEST(isdup(fd1, fd2, 0), ==, 0);
    TEST(reason, >, REASON_GETFL);
    TEST(isdup(fd1, fd3, 0), ==, 0);
    TEST(reason, ==, REASON_GETFL);
    close(fd1);
    close(fd2);
    close(fd3);

    /*
     * Test REASON_FLOCK_{POS,NEG}.
     */
    fd1 = open(filename, O_RDONLY);
    fd2 = open(filename, O_RDONLY);
    fd3 = dup(fd2);
    TEST(isdup(fd1, fd2, 0), ==, 0);
    TEST(reason, ==, REASON_FLOCK_NEG);
    TEST(isdup(fd1, fd3, 0), ==, 0);
    TEST(reason, ==, REASON_FLOCK_NEG);
    TEST(isdup(fd2, fd3, 0), >, 0);
    TEST(reason, ==, REASON_FLOCK_POS);
    close(fd1);
    close(fd2);
    close(fd3);

    /*
     * Test REASON_FLOCK_INCONCLUSIVE (unwillingness to give an
     * approximate answer).
     */
    fd1 = open(filename, O_RDONLY);
    fd2 = open(filename, O_RDONLY);
    fd3 = dup(fd2);
    fd4 = open(filename, O_RDONLY);
    flock(fd4, LOCK_EX);
    TEST(isdup(fd1, fd2, 0), <, 0);
    TEST(reason, ==, REASON_FLOCK_INCONCLUSIVE);
    TEST(isdup(fd1, fd3, 0), <, 0);
    TEST(reason, ==, REASON_FLOCK_INCONCLUSIVE);
    TEST(isdup(fd2, fd3, 0), <, 0);
    TEST(reason, ==, REASON_FLOCK_INCONCLUSIVE);
    close(fd1);
    close(fd2);
    close(fd3);
    close(fd4);

    /*
     * Test REASON_NONBLOCK_{POS,NEG}.
     */
    fd1 = open(filename, O_RDONLY);
    fd2 = open(filename, O_RDONLY);
    fd3 = dup(fd2);
    fd4 = open(filename, O_RDONLY);
    flock(fd4, LOCK_EX);
    TEST(isdup(fd1, fd2, 1), ==, 0);
    TEST(reason, ==, REASON_NONBLOCK_NEG);
    TEST(isdup(fd1, fd3, 1), ==, 0);
    TEST(reason, ==, REASON_NONBLOCK_NEG);
    TEST(isdup(fd2, fd3, 1), >, 0);
    TEST(reason, ==, REASON_NONBLOCK_POS);
    close(fd1);
    close(fd2);
    close(fd3);
    close(fd4);

    /*
     * Remove temporary files.
     */
    remove(filename);
    remove(filename2);

    printf("Passed %d, failed %d\n", passes, fails);

    return fails != 0;
}

#endif

#ifdef STANDALONE_ISDUP

/*
gcc -DSTANDALONE_ISDUP -o isdup isdup.c
*/

#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define REASON_STRING(id, str) str,
static const char *const reason_strings[] = { REASONS(REASON_STRING) };

int main(int argc, char **argv)
{
    int doing_opts = 1;
    int fds[2], nfds = 0;
    int approx = 1;
    int quiet = 0;

    while (--argc > 0) {
        const char *arg = *++argv;
        if (doing_opts && arg[0] == '-' && arg[1]) {
            if (!strcmp(arg, "--")) {
                doing_opts = 0;
            } else if (!strcmp(arg, "--help")) {
                printf("\
usage:   isdup [options] FD FD\n\
options: --strict        exit(64) if definite answer can't be given\n\
         -q, --quiet     print no output; only return 0 for dups, 1 for not\n\
also:    isdup --help    display this text\n\
");
                return 0;
            } else if (!strcmp(arg, "--strict")) {
                approx = 0;
            } else if (!strcmp(arg, "-q") || !strcmp(arg, "--quiet")) {
                quiet = 1;
            } else {
                fprintf(stderr, "isdup: unrecognised option '%s'\n", arg);
                return 2;
            }
        } else {
            if (nfds == 2) {
                fprintf(stderr, "isdup: unexpected additional "
                        "argument '%s'\n", arg);
                return 2;
            } else {
                char *endptr;
                long fd;
                if (!strcmp(arg, "stdin")) {
                    fd = 0;
                } else if (!strcmp(arg, "stdout")) {
                    fd = 1;
                } else if (!strcmp(arg, "stderr")) {
                    fd = 2;
                } else {
                    fd = strtol(arg, &endptr, 0);
                    if (!(*arg && !*endptr && fd >= 0 && fd <= INT_MAX)) {
                        fprintf(stderr, "isdup: unable to parse argument '%s' "
                                "as a file descriptor number\n", arg);
                        return 2;
                    }
                }
                fds[nfds++] = fd;
            }
        }
    }

    if (nfds != 2) {
        fprintf(stderr, "isdup: expected two file descriptors\n");
        return 2;
    }

    int retd = isdup(fds[0], fds[1], approx);

    if (retd < 0) {
        if (!quiet)
            printf("inconclusive: %s\n", reason_strings[reason]);
        return 64;
    } else if (retd == 0) {
        if (!quiet)
            printf("fds %d,%d are not duplicates: %s\n", fds[0], fds[1],
                   reason_strings[reason]);
        return 1;
    } else {
        if (!quiet)
            printf("fds %d,%d are duplicates: %s\n", fds[0], fds[1],
                   reason_strings[reason]);
        return 0;
    }
}

#endif
